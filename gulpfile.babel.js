'use strict';

import gulp from 'gulp';
import webpackStream  from 'webpack-stream';
import nodemon from 'gulp-nodemon';
import babel from 'gulp-babel';

gulp.task('build', () =>
    gulp.src(['./src/**/*.*'])
    .pipe(babel())
    .pipe(gulp.dest('dist/server/'))
);


gulp.task('watch', ['build'], () => {
    gulp.watch(['./src/**/*.*'], ['build']);
    //gulp.start('run');
});

gulp.task('run', () => {
    nodemon({
        delay: 10,
        script: './app.js',
        // args: ["config.json"],
        ext: 'js',
        watch: '.'
    })
});

gulp.task('default', ['build', 'run']);