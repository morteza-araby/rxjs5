import Rx from "rxjs/Rx"
import {createSubscriber} from "./lib/util"
//#################### merge #################################
// //Merge together two Observables: 1s interval and clicks
// var clicks = Rx.Observable.fromEvent(document, 'click')
// var timer = Rx.Observable.interval(1000).take(10)

// Rx.Observable.merge(clicks, timer)
// .subscribe(createSubscriber("merge"))

// //Merge together 3 Observables, but only 2 run concurrently
// var timer1 = Rx.Observable.interval(1000).take(10)
// var timer2 = Rx.Observable.interval(2000).take(6)
// var timer3 = Rx.Observable.interval(500).take(10)
// var concurrent = 2 // the argument
// timer1.merge(timer2, timer3, concurrent)
// .subscribe(createSubscriber('merge2'))

/**
 *#################### mergeAll #################################
Converts a higher-order Observable into a first-order Observable 
which concurrently delivers all values that are emitted on the inner 
Observables. 
Flattens an Observable-of-Observables.
 */
// //Spawn a new interval Observable for each click event, and 
// //blend their outputs as one Observable
// var clicks = Rx.Observable.fromEvent(document, 'click')
// var higherOrder = clicks.map((ev) => Rx.Observable.interval(1000))
// var firstOrder = higherOrder.mergeAll()
// firstOrder.subscribe(createSubscriber('mergeAll'))

// //Count from 0 to 9 every second for each click, but only allow 2 
// //concurrent timers
// var clicks = Rx.Observable.fromEvent(document, 'click')
// var higherOrder = clicks.map((ev) => Rx.Observable.interval(1000).take(10))
// var firstOrder = higherOrder.mergeAll(2)
// firstOrder.subscribe(createSubscriber('mergeAll2'))

/**
 * ############################ mergeMap ##############################
 * Projects each source value to an Observable which is merged in the 
 * output Observable.
 * Maps each value to an Observable, then flattens all of these inner 
 * Observables using mergeAll.
 */
// //Map and flatten each letter to an Observable ticking every 1 second
// var letters = Rx.Observable.of('a', 'b', 'c')
// var result = letters.mergeMap(doMap)
// result.subscribe(createSubscriber('mergeMap'))

// function doMap (x) {
//     return Rx.Observable.interval(1000)
//     .take(10)
//     .map(i => x+i)
// }

/**
 * ######################### mergeMapTo ##############################
 * It's like mergeMap, but maps each value always to the same inner Observable.
 */
var clicks = Rx.Observable.fromEvent(document, 'click')
var result = clicks.mergeMapTo(Rx.Observable.interval(1000).take(10))
result.subscribe(createSubscriber('mergeMapTo'))