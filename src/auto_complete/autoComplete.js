import { Observable } from 'rxjs'
import Rx from "rxjs/Rx"

const $title = $("#title")
const $results = $("#results")

Rx.Observable.fromEvent($title, "keyup")
    .map(e => e.target.value)
    .distinctUntilChanged()
    .debounceTime(250)
    .switchMap(getItems)
    .do(items => console.log("### Got items: ", items))
    .subscribe(
        printResult,
        e => console.error('error ', e),
        () => console.info('complete')
    )

function printResult(items) {
    $results.empty()
    $results.append(items.map(r => $(`<li />`).text(r)))
}

// const keyUps$ = Rx.Observable.fromEvent($title, "keyup")
// const queries$ = keyUps$
//     .map(e => e.target.value)
//     .distinctUntilChanged()
//     .debounceTime(250)
//     //.mergeMap(query => getItems(query)) // flatMap
//     .switchMap(query => getItems(query))

// queries$.subscribe(items => {
//         $results.empty()
//         $results.append(items.map(r => $(`<li />`).text(r)))

// })

//--------------------------
// Library

function getItems(title) {
    console.log("Quering ", title)
    return new Promise((resolve, reject) => {
        window.setTimeout(() => {
            resolve([title, "Item 2", `Another ${Math.random()}`])
        }, 500 + (Math.random() * 2000))
    })
}
