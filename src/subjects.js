import Rx from "rxjs/Rx"
import { createSubscriber } from "./lib/util"


console.log("In subject")

//const simple$ = new Rx.Subject()
// simple$.subscribe(createSubscriber("subject"))

// simple$.next("Hello")
// simple$.next("World")
// simple$.complete()

////Another
// const interval$ = Rx.Observable.interval(1000).take(5)
// const intervalSubject$ = new Rx.Subject()
// interval$.subscribe(intervalSubject$)

// intervalSubject$.subscribe(createSubscriber("sub1"))
// intervalSubject$.subscribe(createSubscriber("sub2"))
// intervalSubject$.subscribe(createSubscriber("sub3"))

// setTimeout(() => {
//     intervalSubject$.subscribe(createSubscriber("LOOK AT ME !!!!!"))
// }, 3000)

///Another
// const currentUser$ = new Rx.Subject()
// const isLoggedIn$ = currentUser$
//     .map(u => u.isLoggedIn)


// isLoggedIn$.subscribe(createSubscriber("isLoggedIn"))

// currentUser$.next({isLoggedIn: false})
// let user = {isLoggedIn: true, name: "nelson"}
// setTimeout(() => {
//     currentUser$.next(user)
    
// }, 2000)

// // currentUser$.subscribe(
// //         item => console.log("two.next",item)
// //     )    

// setTimeout(() => {
//     currentUser$.next({isLoggedIn: false})
// }, 2000)


// currentUser$.subscribe(createSubscriber("createUser"))

///Another
const apiCall$ = new Rx.AsyncSubject()
apiCall$.next(1)

apiCall$.subscribe(createSubscriber("one"))
apiCall$.next(2)


setTimeout(() => {
    apiCall$.subscribe(createSubscriber("two"))
    
}, 2000)


setTimeout(() => {
    apiCall$.complete()    
}, 3000)

