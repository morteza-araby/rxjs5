import Rx from "rxjs/Rx"
import {createSubscriber} from "./lib/util"
//---------- Part 1

// // const promise = new Promise((resolve, reject) => {
// //     console.log("Creating Promise!!!")
// //     setTimeout(() => {
// //         console.log("Promise an item!")
// //         resolve("Promise Resolved!!!")
// //     }, 1000)

// // })

// // promise.then((item) =>{
// //     console.log(item)
// // })

// const simple$ = new Rx.Observable(observer => {
//     console.log("Generating observable")
//     setTimeout(() => {
//         observer.next("An item")
//         setTimeout(() => {
//             observer.next("Another item!!!")
//             observer.complete()
//         }, 1000)
//     }, 1000)
// })

// const error$ = new Rx.Observable(observer => {
//     observer.error(new Error("bufffffff!!"))
// })
// error$.subscribe({
//     next: item => console.log(`one.next ${item}`),
//     error: error =>{
//         console.log(`one.error ${error.stack}`)
//     },
//     complete: () => console.log('one.complete')

// })

// setTimeout(() => {
//     simple$.subscribe(
//         item => console.log(`two.next ${item}`),
//         error => console.log(`two.error ${error}`),
//         () => console.log("two complete")
//     )
// })

// ---------- Part 2 --------------

function createInterval$(time) {
    return new Rx.Observable(observer => {
        let index = 0
        let interval = setInterval(() => {
            console.log("Generating in setInterval: ", index)
            observer.next(index++)
        }, time)

        return () => {
            clearInterval(interval)
        }
    })
}

function take$(sourceObservable, amount) {
    return new Rx.Observable(observer => {
        let count = 0
        const subscription = sourceObservable.subscribe({
            next(item) {
                observer.next(item)
                if (++count >= amount)
                    observer.complete()
            },
            error(error) { observer.error(error) },
            complete() { observer.complete() }
        })

        return () => subscription.unsubscribe()
    })
}

const everySecond$ = createInterval$(1000)
const firstFiveSeconds$ = take$(everySecond$, 5)
const subscription = firstFiveSeconds$.subscribe(createSubscriber('one'))

// setTimeout(() => {
//     subscription.unsubscribe()
// }, 4000)














