import Rx from "rxjs/Rx"
import { createSubscriber } from "./lib/util"
//import fs from "fs"

// fs.readdir("./", (err, items) =>{
//     if(err) console.error(err)
//     else{
//         console.log("Listing:  ",items)
//     }
// })

// const readdir$ = Rx.Observable.bindNodeCallback(fs.readdir)

// readdir$("./")
//     .mergeMap(files => Rx.Observable.from(files))
//     .map(file => `MANIPULATED ${file}`)
//     .subscribe(createSubscriber("readdir"))    


function getItem(){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Hello")
        }, 1000)
    })
}

Rx.Observable.fromPromise(getItem())
    .subscribe(createSubscriber("promise"))