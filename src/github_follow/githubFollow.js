import Rx from "rxjs/Rx"

let refreshClick$ = null
let closeClick1$ = null
let closeClick2$ = null
let closeClick3$ = null
let response$ = null
let suggesstion1$ = null
let suggesstion2$ = null
let suggesstion3$ = null

function runMe() {
    let refreshButton = $('.refresh')
    let closeButton1 = $('.close1')
    let closeButton2 = $('.close2')
    let closeButton3 = $('.close3')
    if (refreshButton.length === 0) return

    refreshClick$ = Rx.Observable.fromEvent(refreshButton, 'click').share()
    closeClick1$ = Rx.Observable.fromEvent(closeButton1, 'click')
    closeClick2$ = Rx.Observable.fromEvent(closeButton2, 'click')
    closeClick3$ = Rx.Observable.fromEvent(closeButton3, 'click')
    if (!refreshClick$ || !closeClick1$ || !closeClick2$ || !closeClick3$) return

    let request$ = refreshClick$.startWith('starup click')
        .map(() => {
            var randomOffset = Math.floor(Math.random() * 500);
            // Just so we don't max out the anonymous github api req
            // limit, I've cached a page. We'll pretend for now.
            // To really hit the API, use
            // 'https://api.github.com/users?since=' + randomOffset;
            return 'users.json'
        })

    response$ = request$.flatMap(load).share()

    suggesstion1$ = createSuggestionStream(closeClick1$)
    suggesstion2$ = createSuggestionStream(closeClick2$)
    suggesstion3$ = createSuggestionStream(closeClick3$)

    suggesstion1$.subscribe((suggestedUser) => {
        renderSuggestion(suggestedUser, '.suggestion1')
    })

    suggesstion2$.subscribe((suggestedUser) => {
        renderSuggestion(suggestedUser, '.suggestion2')
    })

    suggesstion3$.subscribe((suggestedUser) => {
        renderSuggestion(suggestedUser, '.suggestion3')
    })

}
function load(url) {
    return Rx.Observable.create(observer => {
        let xhr = new XMLHttpRequest()
        xhr.addEventListener("load", () => {
            if (xhr.status === 200) {
                let data = JSON.parse(xhr.responseText)
                observer.next(data)
                observer.complete()
            } else {
                observer.error(xhr.statusText)
            }
        })
        xhr.open("GET", url)
        xhr.send()
    }).retryWhen(retryStrategy({ attempts: 3, delay: 1500 }))
}
function retryStrategy({attempts = 4, delay = 1000}) {
    return function (errors) {
        return errors
            .scan((acc, value) => {
                console.log(acc, value)
                return acc + 1
            }, 0)
            .takeWhile(acc => acc < attempts)
            .delay(delay)
    }
}

function getRandomUser(click, listUsers) {
    return listUsers[Math.floor(Math.random() * listUsers.length)]
}

function createSuggestionStream(closeClick$) {
    return closeClick$.startWith('startup click')
        .combineLatest(response$, getRandomUser)
        .merge(refreshClick$.map(() => {
            return null
        }))
        .startWith(null)
}

// Rendering
function renderSuggestion(suggestedUser, selector) {
    let suggstionEl = document.querySelector(selector)
    if (suggestedUser === null) {
        suggstionEl.style.visibility = "hidden"
    } else {
        suggstionEl.style.visibility = 'visible'
        let usernameEl = suggstionEl.querySelector('.username')
        usernameEl.href = suggestedUser.html_url
        usernameEl.textContent = suggestedUser.login
        var imgEl = suggstionEl.querySelector('img')
        imgEl.src = ''
        imgEl.src = suggestedUser.avatar_url
    }
}

runMe()