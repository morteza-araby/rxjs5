import { Observable } from 'rxjs'

function runMe() {
    let button = document.getElementById("render-movie-button")
    let output = document.getElementById("render-movie-output")
    if(!button || !output) return

    let click = Observable.fromEvent(button, "click")

    // click.flatMap(e=> load("movies.json"))
    // .subscribe(o => console.log(o))

    load("movies.json").subscribe(renderMovies)

    click.flatMap(e => loadWithFetch("movies.json"))
        .subscribe(
        renderMovies,
        e => console.log(`error: ${e}`),
        () => console.log('complete')
        )

}

function load(url) {
    return Observable.create(observer => {
        let xhr = new XMLHttpRequest()
        xhr.addEventListener("load", () => {
            if (xhr.status === 200) {
                let data = JSON.parse(xhr.responseText)
                observer.next(data)
                observer.complete()
            } else {
                observer.error(xhr.statusText)
            }
        })
        xhr.open("GET", url)
        xhr.send()
    }).retryWhen(retryStrategy({ attempts: 3, delay: 1500 }))
}

function loadWithFetch(url) {
    return Observable.fromPromise(fetch(url)
        .then(r => r.json())
    )
}

function retryStrategy({attempts = 4, delay = 1000}) {
    return function (errors) {
        return errors
            .scan((acc, value) => {
                console.log(acc, value)
                return acc + 1
            }, 0)
            .takeWhile(acc => acc < attempts)
            .delay(delay)
    }
}
function renderMovies(movies) {
    let output = document.getElementById("render-movie-output")
    movies.forEach(m => {
        let div = document.createElement("div")
        div.innerText = m.title
        output.appendChild(div)
    })
}

runMe()