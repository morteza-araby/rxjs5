import { Observable } from 'rxjs'

function runMe() {
    let output = document.getElementById("multi-single-results")
    let btn = document.querySelector("#multi-single-clickMe");
    if (!btn || !output) return
        
    let clickStream = Observable.fromEvent(btn, "click");

    let multiClickStream = clickStream
        .bufferWhen(() => clickStream.debounceTime(250))
        //.debounceTime(250)
        .map((list) => list.length)
        .filter((x) => x >= 2)

    multiClickStream.subscribe((numclicks) => {
        sendValue(numclicks + 'x click')
    })

    let singleClickStream = clickStream
        .bufferWhen(() => clickStream.debounceTime(250))
        .map((list) => list.length)
        .filter((x) => x === 1)

    singleClickStream.subscribe((numclicks) => {
        sendValue(numclicks + 'x click')
    })

}

function sendValue(arr) {
    let pre = document.createElement("pre")
    pre.innerHTML = JSON.stringify(arr)
    document.querySelector("#multi-single-results").appendChild(pre)
}

runMe()





