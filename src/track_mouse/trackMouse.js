//http://www.jacklmoore.com/notes/mouse-position/
//https://www.kirupa.com/html5/get_element_position_using_javascript.htm


import Rx from "rxjs/Rx"
var socket = io('http://localhost:1340');

function init() {
    socket.on('connected', (msg) => {
        console.log('connected: ', msg);
        let result = JSON.stringify(msg);
    });
    // let sender = document.getElementById('sender')
    // if (!sender.checked) {
    //     socket.on('chat message', function (msg) {
    //         let value = msg;
    //         console.log('##RECEIVED: ', msg)
    //         let circle = document.getElementById('circle')
    //         if (!circle) return
    //         circle.style.left = "" + value.x + "px"
    //         circle.style.top = "" + value.y + "px"
    //         console.log("### Emitted", value)
    //         socket.emit('chat message', value)
    //     });
    // }
}

function runMe() {
    let circle = document.getElementById('circle')
    if (!circle) return
    let source = Rx.Observable.fromEvent(document, 'mousemove')
        .map(e => {
            return {
                x: e.clientX,
                y: e.clientY
            }
        })
        .delay(300)

    source.subscribe(
        onNext,
        e => console.error('error ', e),
        () => console.info('complete')
    )
}


function onNext(value) {
    if (!circle) return
    circle.style.left = "" + value.x + "px"
    circle.style.top = "" + value.y + "px"
    console.log("### Emitted", value)
    socket.emit('message', value)

}

init()
let sender = $('#sender')
sender.change((event) => {    
    if(event.target.checked){
        runMe()
    }
})

let receiver = $('#receiver')
receiver.change((event) => {
    if(event.target.checked){
         socket.on('message', function (msg) {
            let value = msg;
            console.log('##RECEIVED: ', msg)
            let circle = document.getElementById('circle')
            if (!circle) return
            circle.style.left = "" + value.x + "px"
            circle.style.top = "" + value.y + "px"
            console.log("### Emitted", value)
            socket.emit('message', value)
        });
    }
})
