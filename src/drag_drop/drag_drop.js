import Rx from 'rxjs/Rx'

var drops$ = null
var $drag = null

function runMe() {
    $drag = $('#drag')
    const $document = $(document)
    const $dropArea = $('.drop-area')
    if(!$drag) return
    const beginDrag$ = Rx.Observable.fromEvent($drag, 'mousedown')
    const endDrag$ = Rx.Observable.fromEvent($document, 'mouseup')
    const mouseMove$ = Rx.Observable.fromEvent($document, 'mousemove')

    const currentOverArea$ = Rx.Observable.merge(
        Rx.Observable.fromEvent($dropArea, "mouseover").map(e => $(e.target)),
        Rx.Observable.fromEvent($dropArea, "mouseout").map(e => null)

    )

    drops$ = beginDrag$
        .do(e => {
            e.preventDefault()
            $drag.addClass('dragging')
        })
        .mergeMap(startEvent => {
            return mouseMove$
                .takeUntil(endDrag$)
                .do(MoveEvent => moveDrag(startEvent, MoveEvent))
                .last()
                .withLatestFrom(currentOverArea$, (_, $area) => $area)
        })
        .do(() => {
            $drag.removeClass('dragging')
                .animate({ top: 0, left: 0 }, 250)
        })

    drops$.subscribe($dropArea => {
        if ($dropArea) {
            $dropArea.removeClass('dropped')
            $dropArea.addClass('dropped')
        }
    })

}


function moveDrag(startEvent, moveEvent) {
    $drag.css({
        left: moveEvent.clientX - startEvent.offsetX,
        top: moveEvent.clientY - startEvent.offsetY
    })
}

runMe()